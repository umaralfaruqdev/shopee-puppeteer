const puppeteer = require("puppeteer-core");
const options = require("./../config/options.json")

// init
const _LINKS = [options.product.link, options.product.direct]

const App = async () => {
    let browser = await puppeteer.launch(options.opts);

    // close the empty pages
    browser.on('targetcreated', async function f() {
        let pages = await browser.pages();
        if (pages.length > 1) {
            await pages[0].close();

            browser.off('targetcreated', f);
        }
    });
    openTab(browser); // add to cart
}

const openTab = async (browser) => {
    try {

        let payPage = await browser.newPage(); // instance page of payment
        let productPage = await browser.newPage(); // instance page

        await productPage.goto(_LINKS[0], { waitUntil: 'networkidle2' }); // goto product
        await payPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto direct payment
        await payPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto direct payment
        await payPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto direct payment
        await payPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto direct payment
        await payPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto direct payment
        await payPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto direct payment

        // add to cart
        atc(browser, productPage, payPage);

    } catch(err) {
        console.log(err);
    }
}

const atc = async (browser, productPage, payPage) => {

    console.time('atc'); // start timer

    // add to cart
    await productPage.evaluate(() => {
        const atcBtn = document.querySelector('.product-bottom-panel__add-to-cart').click();
    });

    console.timeEnd('atc') // end timer

    // payment
    payment(browser, payPage, productPage);
}

const payment = async (browser, payPage, productPage) => {
    console.time('payment'); // start time

    await payPage.bringToFront(); // get active page
    await payPage.goto(_LINKS[1], { waitUntil: 'networkidle0' });
    await payPage.evaluate(() => {
        document.querySelector('.page-checkout-place-order-section__button').click()
    });
    console.timeEnd('payment'); // end time

    await productPage.close()
    await payPage.screenshot({path: 'public/buddy-screenshot.png'});
    await browser.close();
}

// running the app
App()

