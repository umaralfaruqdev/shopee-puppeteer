const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());
const options = require("./../config/options.json")

// init
const _LINKS = ["https://m.facebook.com/", "https://shopee.co.id/buyer/login"]

const App = async () => {
    let browser = await puppeteer.launch(options.opts);

    // close the empty pages
    browser.on('targetcreated', async function f() {
        let pages = await browser.pages();
        if (pages.length > 1) {
            await pages[0].close();

            browser.off('targetcreated', f);
            console.log(pages.length)
        }
    });
    login(browser); // add to cart
}

const login = async (browser) => {
    try {

        let shopeLoginPage = await browser.newPage(); // shopee login
        await shopeLoginPage.goto(_LINKS[1], { waitUntil: 'networkidle2' }); // goto login

        console.time('start')

        await shopeLoginPage.evaluate(() => {
            document.querySelector('.JrGzAG').click()
        })

        const newPagePromise = new Promise(x => browser.once('targetcreated', target => x(target.page()))); 
        const popup = await newPagePromise;
        if (popup) {
            console.log("on facebook page")
            await popup.waitForSelector('[name="email"]')

            await popup.type('[name="email"]', '085776699829')
            await popup.type('[name="pass"]', 'umardev155')

            await popup.screenshot({path: 'public/sc.png'});

            await popup.click('[name="login"]');

            await popup.waitForSelector('[name="__CONFIRM__"]');
            await popup.screenshot({path: 'public/scd2.png'});
            // await popup.click('[name="__CONFIRM__"]');
        } else {
            console.log(popup)
        }

        // await shopeLoginPage.waitForSelector('._1y306T');
        // await shopeLoginPage.screenshot({path: '../public/screenshot.png'});

        console.timeEnd('start')

        await browser.close()


    } catch(err) {
        console.log(err);
    }
}

// running the app
App()

